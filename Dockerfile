FROM ubuntu:18.04

LABEL maintainer Alexander Merck <alexander.t.merck@gmail.com>
LABEL name "wordpot"
LABEL version "0.2"
LABEL release "1"
LABEL summary "Wordpot Honeypot container"
LABEL description "Wordpot is a Wordpress honeypot which detects probes for plugins, themes, timthumb and other common files used to fingerprint a wordpress installation."
LABEL authoritative-source-url "https://github.com/CommunityHoneyNetwork/wordpot"
LABEL changelog-url "https://github.com/CommunityHoneyNetwork/wordpot/commits/master"

# Set DOCKER var - used by Wordpot init to determine logging
ENV DOCKER "yes"
ENV playbook "wordpot.yml"

RUN apt-get update \
      && apt-get install -y ansible python-apt

RUN echo "localhost ansible_connection=local" >> /etc/ansible/hosts
ADD . /opt/
RUN ansible-playbook /opt/${playbook}

ENTRYPOINT ["/usr/bin/runsvdir", "-P", "/etc/service"]
